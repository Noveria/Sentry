package me.linuxsquare.sentry.utils

import me.linuxsquare.sentry.Sentry
import net.md_5.bungee.api.*
import net.md_5.bungee.api.chat.BaseComponent
import net.md_5.bungee.api.chat.ComponentBuilder
import net.md_5.bungee.api.config.ServerInfo
import net.md_5.bungee.api.connection.Connection
import net.md_5.bungee.api.connection.PendingConnection
import net.md_5.bungee.api.connection.ProxiedPlayer
import net.md_5.bungee.api.connection.Server
import net.md_5.bungee.api.event.ServerConnectEvent
import net.md_5.bungee.api.score.Scoreboard
import java.net.InetSocketAddress
import java.net.SocketAddress
import java.util.*

open class SentryPlayer(id: UUID): ISentryPlayer {

    private val proxiedPlayer: ProxiedPlayer
    private val sentry: Sentry

    init {
        proxiedPlayer = ProxyServer.getInstance().getPlayer(id)
        sentry = ProxyServer.getInstance().pluginManager.getPlugin("Sentry") as Sentry
    }

    override fun isJailed(): Boolean {
        return sentry.getDatabaseModel().isJailed(SentryPlayer(proxiedPlayer.uniqueId))
    }

    @Deprecated("'getter for address: InetSocketAddress!' is deprecated. Deprecated in Java")
    override fun getAddress(): InetSocketAddress {
        return proxiedPlayer.address
    }

    override fun getSocketAddress(): SocketAddress {
        return proxiedPlayer.socketAddress
    }

    @Deprecated("'disconnect(String!): Unit' is deprecated. Deprecated in Kotlin")
    override fun disconnect(reason: String?) {
        proxiedPlayer.disconnect(reason)
    }

    override fun disconnect(vararg reason: BaseComponent?) {

        val builder = ComponentBuilder()
        for(res in reason) {
            builder.append(res)
        }

        proxiedPlayer.disconnect(builder.currentComponent)
    }

    override fun disconnect(reason: BaseComponent?) {
        proxiedPlayer.disconnect(reason)
    }

    override fun isConnected(): Boolean {
        return proxiedPlayer.isConnected
    }

    override fun unsafe(): Connection.Unsafe {
        return proxiedPlayer.unsafe()
    }

    override fun getName(): String {
        return proxiedPlayer.name
    }

    override fun sendMessage(position: ChatMessageType?, vararg message: BaseComponent?) {

        val builder = ComponentBuilder()
        for(msg in message) {
            builder.append(msg)
        }

        proxiedPlayer.sendMessage(position, builder.currentComponent)
    }

    override fun sendMessage(position: ChatMessageType?, message: BaseComponent?) {
        proxiedPlayer.sendMessage(position, message)
    }

    override fun sendMessage(sender: UUID?, vararg message: BaseComponent?) {

        val builder = ComponentBuilder()
        for(msg in message) {
            builder.append(msg)
        }

        proxiedPlayer.sendMessage(sender, builder.currentComponent)
    }

    override fun sendMessage(sender: UUID?, message: BaseComponent?) {
        proxiedPlayer.sendMessage(sender, message)
    }

    @Deprecated("'sendMessage(String!): Unit' is deprecated. Deprecated in Kotlin")
    override fun sendMessage(message: String?) {
        proxiedPlayer.sendMessage(message)
    }

    override fun sendMessage(vararg message: BaseComponent?) {

        val builder = ComponentBuilder()
        for(msg in message) {
            builder.append(msg)
        }

        proxiedPlayer.sendMessage(builder.currentComponent)
    }

    override fun sendMessage(message: BaseComponent?) {
        proxiedPlayer.sendMessage(message)
    }

    @Deprecated("'sendMessage(String!): Unit' is deprecated. Deprecated in Kotlin")
    override fun sendMessages(vararg messages: String?) {
        val builder = ComponentBuilder()
        for(msg in messages) {
            builder.append(msg)
        }

        proxiedPlayer.sendMessage(builder.currentComponent.toLegacyText())
    }

    override fun getGroups(): MutableCollection<String> {
        return proxiedPlayer.groups
    }

    override fun addGroups(vararg groups: String?) {
        val sb = StringBuilder()
        for(group in groups) {
            sb.append(group)
        }

        proxiedPlayer.addGroups(sb.toString())
    }

    override fun removeGroups(vararg groups: String?) {
        val sb = StringBuilder()
        for(group in groups) {
            sb.append(group)
        }

        proxiedPlayer.removeGroups(sb.toString())
    }

    override fun hasPermission(permission: String?): Boolean {
        return proxiedPlayer.hasPermission(permission)
    }

    override fun setPermission(permission: String?, value: Boolean) {
        proxiedPlayer.setPermission(permission, value)
    }

    override fun getPermissions(): MutableCollection<String> {
        return proxiedPlayer.permissions
    }

    override fun getDisplayName(): String {
        return proxiedPlayer.displayName
    }

    override fun setDisplayName(name: String?) {
        proxiedPlayer.displayName = name
    }

    override fun connect(target: ServerInfo?) {
        proxiedPlayer.connect(target)
    }

    override fun connect(target: ServerInfo?, reason: ServerConnectEvent.Reason?) {
        proxiedPlayer.connect(target, reason)
    }

    override fun connect(target: ServerInfo?, callback: Callback<Boolean>?) {
        proxiedPlayer.connect(target, callback)
    }

    override fun connect(target: ServerInfo?, callback: Callback<Boolean>?, retry: Boolean) {
        proxiedPlayer.connect(target, callback, retry)
    }

    override fun connect(target: ServerInfo?, callback: Callback<Boolean>?, retry: Boolean, timeout: Int) {
        proxiedPlayer.connect(target, callback, retry, timeout)
    }

    override fun connect(target: ServerInfo?, callback: Callback<Boolean>?, reason: ServerConnectEvent.Reason?) {
        proxiedPlayer.connect(target, callback, reason)
    }

    override fun connect(
        target: ServerInfo?,
        callback: Callback<Boolean>?,
        retry: Boolean,
        reason: ServerConnectEvent.Reason?,
        timeout: Int
    ) {
        proxiedPlayer.connect(target, callback, retry, reason, timeout)
    }

    override fun connect(request: ServerConnectRequest?) {
        proxiedPlayer.connect(request)
    }

    override fun getServer(): Server {
        return proxiedPlayer.server
    }

    override fun getPing(): Int {
        return proxiedPlayer.ping
    }

    override fun sendData(channel: String?, data: ByteArray?) {
        proxiedPlayer.sendData(channel, data)
    }

    override fun getPendingConnection(): PendingConnection {
        return proxiedPlayer.pendingConnection
    }

    override fun chat(message: String?) {
        proxiedPlayer.chat(message)
    }

    override fun getReconnectServer(): ServerInfo {
        return proxiedPlayer.reconnectServer
    }

    override fun setReconnectServer(server: ServerInfo?) {
        proxiedPlayer.reconnectServer = server
    }

    @Deprecated("'getter for uuid: String!' is deprecated. Deprecated in Kotlin")
    override fun getUUID(): String {
        return proxiedPlayer.uuid
    }

    override fun getUniqueId(): UUID {
        return proxiedPlayer.uniqueId
    }

    override fun getLocale(): Locale {
        return proxiedPlayer.locale
    }

    override fun getViewDistance(): Byte {
        return proxiedPlayer.viewDistance
    }

    override fun getChatMode(): ProxiedPlayer.ChatMode {
        return proxiedPlayer.chatMode
    }

    override fun hasChatColors(): Boolean {
        return proxiedPlayer.hasChatColors()
    }

    override fun getSkinParts(): SkinConfiguration {
        return proxiedPlayer.skinParts
    }

    override fun getMainHand(): ProxiedPlayer.MainHand {
        return proxiedPlayer.mainHand
    }

    override fun setTabHeader(header: BaseComponent?, footer: BaseComponent?) {
        proxiedPlayer.setTabHeader(header, footer)
    }

    override fun setTabHeader(header: Array<out BaseComponent>?, footer: Array<out BaseComponent>?) {
        proxiedPlayer.setTabHeader(header, footer)
    }

    override fun resetTabHeader() {
        proxiedPlayer.resetTabHeader()
    }

    override fun sendTitle(title: Title?) {
        proxiedPlayer.sendTitle(title)
    }

    override fun isForgeUser(): Boolean {
        return proxiedPlayer.isForgeUser
    }

    override fun getModList(): MutableMap<String, String> {
        return proxiedPlayer.modList
    }

    override fun getScoreboard(): Scoreboard {
        return proxiedPlayer.scoreboard
    }
}