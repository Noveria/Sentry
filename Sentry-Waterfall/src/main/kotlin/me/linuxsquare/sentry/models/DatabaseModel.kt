package me.linuxsquare.sentry.models

import me.linuxsquare.sentry.Sentry
import me.linuxsquare.sentry.utils.SentryPlayer
import net.md_5.bungee.api.ProxyServer
import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet

class DatabaseModel(sentry: Sentry) {

    private val Bungee: ProxyServer = ProxyServer.getInstance()
    private val sentry: Sentry
    private lateinit var con: Connection

    init {
        this.sentry = sentry
    }

    fun connect() {
        val host = sentry.getConfigModel().getConfig().getString("Database.host")
        val port = sentry.getConfigModel().getConfig().getString("Database.port")
        val dbname = sentry.getConfigModel().getConfig().getString("Database.DBName")
        val username = sentry.getConfigModel().getConfig().getString("Database.username")
        val password = sentry.getConfigModel().getConfig().getString("Database.password")

        if(username.equals("<YourDBUserHere>", true) || password.equals("<YourDBPasswordHere>", true)) {
            Bungee.logger.info("${Sentry.PREFIX}§ePlease update your database credentials and then restart your server.")
            return
        }

        var dburl = "jdbc:mysql://${host}:${port}"
        setCon(DriverManager.getConnection(dburl,username,password))

        val sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '${dbname}';"
        val stmt = getCon().createStatement()
        var exists = stmt.executeQuery(sql).next()
        if(!exists) {
            Bungee.logger.info("${Sentry.PREFIX}§e'${dbname}' doesn't exist yet. Be sure you've started the spigot at least plugin once!")
            return
        } else {
            Bungee.logger.info("${Sentry.PREFIX}§aDatabase '${dbname}' found. Hooking into it...")
        }
        getCon().close()
        dburl = "jdbc:mysql://${host}:${port}/${dbname}"

        Class.forName("com.mysql.jdbc.Driver")
        setCon(DriverManager.getConnection(dburl,username,password))
        exists = getCon().createStatement().executeQuery("SELECT * FROM information_schema.tables WHERE table_schema = '$dbname' AND table_name = 'banList' LIMIT 1;").next()
        if(!exists) {
            Bungee.logger.info("${Sentry.PREFIX}§e'${dbname}' doesn't exist yet. Be sure you've started the spigot at least plugin once!")
            return
        }
    }

    fun close() {
        if(!this::con.isInitialized) {
            Bungee.logger.info(Sentry.PREFIX + "§cThe database connection was never initialised and is therefore not closed!")
            return
        }

        getCon().close()
    }

    fun getJail(): ResultSet? {

        if(!this::con.isInitialized) {
            Bungee.logger.info(Sentry.PREFIX + "§cThe database connection was never initialised!")
            return null
        }

        val getJail = "SELECT * FROM jailData WHERE id = 1;"
        val stmt = getCon().createStatement()
        val rs = stmt.executeQuery(getJail)
        if(rs.next()) {
            return rs
        }
        return null
    }

    fun isJailed(sentryPlayer: SentryPlayer): Boolean {

        if(!this::con.isInitialized) {
            Bungee.logger.info(Sentry.PREFIX + "§cThe database connection was never initialised!")
            return false
        }

        val isJailed = "SELECT uuid FROM jailList WHERE uuid = '${sentryPlayer.uniqueId}';"
        val stmt = getCon().createStatement()
        return stmt.executeQuery(isJailed).next()
    }

    private fun getCon(): Connection {
        return this.con
    }

    private fun setCon(con: Connection) {
        this.con = con
    }
}