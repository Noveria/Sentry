package me.linuxsquare.sentry

import me.linuxsquare.sentry.commands.*
import me.linuxsquare.sentry.listeners.*
import me.linuxsquare.sentry.models.BungeeCommunicator
import me.linuxsquare.sentry.models.ConfigModel
import me.linuxsquare.sentry.models.DatabaseModel
import me.linuxsquare.sentry.models.HTTPCommunicator
import me.linuxsquare.sentry.utils.AFKHandler
import me.linuxsquare.sentry.utils.JailHandler
import me.linuxsquare.sentry.utils.SentryOfflinePlayer
import me.linuxsquare.sentry.utils.SentryPlayer
import org.bukkit.Bukkit
import org.bukkit.plugin.PluginManager
import org.bukkit.plugin.java.JavaPlugin
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/*
    Sentry
    Copyright (c) 2023 LinuxSquare

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Affero General Public License for more details.

    How to contact me:
    E-Mail: linuxsquare@noveria.org
 */

/*
=============================================================================================================

                        DISCLAIMER
    If you purchased this plugin from anywhere other than Spigot or Bukkit,
    I'm afraid that you have been ripped off.
    Unfortunately, I can't do anything about that,
    as the plugin is released under the GNU AGPLv3 and anyone is free (as in freedom)
    to take the plugin and sell it, redistribute it, etc.

    However, if the purchase page did not mention a direct link to the public GitLab repository,
    (or if modified: the public git-repo of the modified version of this plugin),
    I would be happy if you could send me the page where you bought the plugin.

    See contact above on how you can contact me.

    Thank you in advance
    ~ LinuxSquare

=============================================================================================================
 */

class Sentry: JavaPlugin() {

    companion object {
        const val PREFIX: String = "§e[Sentry]§r "
        private lateinit var plugin: Sentry

        // Yes, those are turret quotes of Portal 2.
        // I thought, they would fit quite nicely in, if the plugin's name is "Sentry"
        private val WELCOMEMSG = arrayOf("Activated", "There you are", "Target aquired.", "Deploying", "Hello!", "Sentry mode activated")
        private val SHUTDOWNMSG = arrayOf("Shutting down", "Nap time", "Goodbye!", "Target lost", "Goodnight", "Resting")

        fun getSentryOfflinePlayer(id: UUID): SentryOfflinePlayer {
            return SentryOfflinePlayer(id)
        }

        fun getSentryPlayer(id: UUID): SentryPlayer {
            return SentryPlayer(id)
        }

        fun getJailedPlayers(): ArrayList<SentryOfflinePlayer> {
            val offlinePlayers = Bukkit.getOfflinePlayers()

            val jailedPlayers: ArrayList<SentryOfflinePlayer> = ArrayList()
            for(op in offlinePlayers) run {
                val sp: SentryOfflinePlayer = getSentryOfflinePlayer(op.uniqueId)
                if(sp.isJailed()) run {
                    jailedPlayers.add(sp)
                }
            }
            return jailedPlayers
        }

        fun getPlugin(): Sentry {
            return plugin
        }
    }

    init {
        plugin = this
    }

    private val httpcom: HTTPCommunicator = HTTPCommunicator()
    private val configModel: ConfigModel = ConfigModel(this)
    private val databaseModel: DatabaseModel = DatabaseModel(this)
    private val jailHandler: JailHandler = JailHandler(this)
    private val afkHandler: AFKHandler = AFKHandler(this)
    private val bungeecom: BungeeCommunicator = BungeeCommunicator()
    private val bungeeListener: BungeeListener = BungeeListener(this)

    override fun onEnable() {
        Bukkit.getConsoleSender().sendMessage(PREFIX + "§a${WELCOMEMSG.random()}")
        this.server.messenger.registerIncomingPluginChannel(this, "BungeeCord", bungeeListener)
        this.server.messenger.registerOutgoingPluginChannel(this, "BungeeCord")

        configModel.loadConfig()
        databaseModel.connect()
        databaseModel.databaseRefresher()
        jailHandler.start()
        afkHandler.start()

        getCommand("sentry")?.setExecutor(SentryCommand(this))
        getCommand("ban")?.setExecutor(BanCommand(this))
        getCommand("banip")?.setExecutor(BanIPCommand(this))
        getCommand("tempban")?.setExecutor(TempbanCommand(this))
        getCommand("unban")?.setExecutor(UnbanCommand(this))
        getCommand("unbanip")?.setExecutor(UnbanIPCommand(this))
        getCommand("kick")?.setExecutor(KickCommand(this))
        getCommand("mute")?.setExecutor(MuteCommand(this))
        getCommand("unmute")?.setExecutor(UnmuteCommand(this))
        getCommand("jail")?.setExecutor(JailCommand(this))
        getCommand("unjail")?.setExecutor(UnjailCommand(this))
        getCommand("jailstatus")?.setExecutor(JailStatusCommand(this))

        val pm: PluginManager = Bukkit.getPluginManager()
        pm.registerEvents(BannedPlayerJoinListener(this), this)
        pm.registerEvents(MutedPlayerChatListener(this), this)
        pm.registerEvents(JailedPlayerJoinListener(this), this)
        pm.registerEvents(PlayerInteractionsListener(), this)

        if(configModel.getConf().getBoolean("Settings.BungeeEnabled")) {
            if (configModel.getConf().getString("BungeeCommunicator.apikey").equals("change_me", true)
                || configModel.getConf().getString("BungeeCommunicator.host").equals("change_me", true)) {
                Bukkit.getConsoleSender().sendMessage("${PREFIX}§cplease update 'change_me' of section BungeeCommunicator to the accurate values, then restart the server again!")
                pm.disablePlugin(this)
            }
            if(configModel.getConf().getString("Settings.BungeeJailServerName").equals("change_me", true)) {
                Bukkit.getConsoleSender().sendMessage("${PREFIX}§cplease update 'change_me' of section BungeeJailServerName to the accurate values, then restart the server again!")
                pm.disablePlugin(this)
            }
        }
    }

    override fun onDisable() {
        databaseModel.stopDatabaseRefresher()
        databaseModel.close()
        jailHandler.stop()
        afkHandler.stop()
        Bukkit.getConsoleSender().sendMessage(PREFIX + "§c${SHUTDOWNMSG.random()}")
    }

    fun getUniqueID(playername: String): String? {
        return httpcom.HTTPGet(playername)
    }

    fun callBungeeGET(path: String): HashMap<String, Any>? {
        return bungeecom.httpGet(path)
    }

    fun callBungeePOST(path: String, params: HashMap<String, Any?>) {
        bungeecom.httpPost(path, params)
    }

    fun getConfigModel(): ConfigModel {
        return this.configModel
    }

    fun getDatabaseModel(): DatabaseModel {
        return this.databaseModel
    }

    fun getBungeeListener(): BungeeListener {
        return this.bungeeListener
    }

}