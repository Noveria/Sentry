package me.linuxsquare.sentry.listeners

import me.linuxsquare.sentry.Sentry
import me.linuxsquare.sentry.models.FileEntry
import me.linuxsquare.sentry.models.FileEntryType
import org.bukkit.Bukkit
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerChatEvent
import org.bukkit.event.player.PlayerCommandPreprocessEvent
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class MutedPlayerChatListener(private val sentry: Sentry): Listener {

    @EventHandler
    fun onMutedPlayerChat(e: AsyncPlayerChatEvent) {
        val sentryplayer = Sentry.getSentryOfflinePlayer(e.player.uniqueId)
        if(sentryplayer.isMuted()) {
            val now: LocalDateTime = LocalDateTime.now()
            val ends: LocalDateTime = LocalDateTime.parse(sentryplayer.getDuration())
            if(now.isAfter(ends)) {
                sentryplayer.unmutePlayer()
                e.player.sendMessage("${Sentry.PREFIX}§aYou have been unmuted, now behave!")
                return
            }
            e.isCancelled = true
            Bukkit.getConsoleSender().sendMessage("${Sentry.PREFIX}§e${e.player.name} tried to write: §6${e.message}")
            e.player.sendMessage("${Sentry.PREFIX}§cYou have been muted until §e${ends.format(DateTimeFormatter.ofPattern(sentry.getConfigModel().getConf().getString("Settings.dateFormatPattern")))} ${sentry.getConfigModel().getConf().getString("Settings.timezone")} §cfor §e${sentryplayer.getReason()}")
        }
    }

    @EventHandler
    fun onMutedPlayerCommand(e: PlayerCommandPreprocessEvent) {
        val sentryplayer = Sentry.getSentryOfflinePlayer(e.player.uniqueId)
        if(sentryplayer.isMuted()) {
            val now: LocalDateTime = LocalDateTime.now()
            val ends: LocalDateTime = LocalDateTime.parse(sentryplayer.getDuration())
            if(now.isAfter(ends)) {
                sentryplayer.unmutePlayer()
                sentryplayer.addFileEntry(FileEntry(
                    sentryplayer.uniqueId,
                    LocalDate.now().toString(),
                    FileEntryType.UNMUTE,
                    "${sentryplayer.name} unmuted")
                )
                e.player.sendMessage("${Sentry.PREFIX}§aYou have been unmuted, now behave!")
                return
            }
            val disallowedCommands: List<String> = sentry.getConfigModel().getConf().getStringList("Settings.disallowedCommandsWhenMuted")
            for(disallowedCommand in disallowedCommands) {
                val split: List<String> = e.message.lowercase().split(" ")
                if(split[0] == disallowedCommand) {
                    e.isCancelled = true
                    Bukkit.getConsoleSender().sendMessage("${Sentry.PREFIX}§e${e.player.name} tried to execute: §6${e.message}")
                    e.player.sendMessage("${Sentry.PREFIX}§cYou have been muted until §e${ends.format(DateTimeFormatter.ofPattern(sentry.getConfigModel().getConf().getString("Settings.dateFormatPattern")))} ${sentry.getConfigModel().getConf().getString("Settings.timezone")} §cfor §e${sentryplayer.getReason()}")
                }
            }
        }
    }

}