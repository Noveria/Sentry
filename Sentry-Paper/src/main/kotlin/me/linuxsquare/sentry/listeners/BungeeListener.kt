package me.linuxsquare.sentry.listeners

import com.google.common.io.ByteArrayDataInput
import com.google.common.io.ByteArrayDataOutput
import com.google.common.io.ByteStreams
import me.linuxsquare.sentry.Sentry
import org.bukkit.entity.Player
import org.bukkit.plugin.messaging.PluginMessageListener

class BungeeListener(private val sentry: Sentry): PluginMessageListener {

    private lateinit var input: ByteArrayDataInput
    private val output: ByteArrayDataOutput = ByteStreams.newDataOutput()

    override fun onPluginMessageReceived(channel: String, player: Player, message: ByteArray) {
        if(channel != "BungeeCord") return

        input = ByteStreams.newDataInput(message)
    }

    fun connectServer(initiator: Player, server: String) {
        output.writeUTF("Connect")
        output.writeUTF(server)

        initiator.sendPluginMessage(sentry, "BungeeCord", output.toByteArray())
    }
}