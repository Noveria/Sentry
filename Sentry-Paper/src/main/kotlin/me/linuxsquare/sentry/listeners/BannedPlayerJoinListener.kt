package me.linuxsquare.sentry.listeners

import me.linuxsquare.sentry.Sentry
import me.linuxsquare.sentry.models.FileEntry
import me.linuxsquare.sentry.models.FileEntryType
import net.kyori.adventure.text.Component
import org.bukkit.Bukkit
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerPreLoginEvent
import org.bukkit.event.player.PlayerLoginEvent
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class BannedPlayerJoinListener(private val sentry: Sentry): Listener {

    @EventHandler
    fun onBannedPlayerJoin(e: PlayerLoginEvent) {
        val sentryplayer = Sentry.getSentryOfflinePlayer(e.player.uniqueId)
        if(sentryplayer.isBanned || sentryplayer.isIPBanned()) {
            if (!sentryplayer.isBanned && sentryplayer.isIPBanned()) {
                // Add potential multiaccount to banList
                sentryplayer.banPlayer("CONSOLE", "Multiaccounts of banned IP's are prohibited")
                e.disallow(PlayerLoginEvent.Result.KICK_BANNED, Component.text("${Sentry.PREFIX}\n" +
                        "§cYou have been banned from this network\n" +
                        "§6From: §c${sentryplayer.getInitiator()}\n" +
                        "§6Reason: §c${sentryplayer.getReason()}\n" +
                        "§6Duration: §cPERMANENT"))
                return
            }
            if(sentryplayer.getDuration().equals("PERMANENT", true)) {
                e.disallow(PlayerLoginEvent.Result.KICK_BANNED, Component.text("${Sentry.PREFIX}\n" +
                        "§cYou have been banned from this network\n" +
                        "§6From: §c${sentryplayer.getInitiator()}\n" +
                        "§6Reason: §c${sentryplayer.getReason()}\n" +
                        "§6Duration: §cPERMANENT"))
            } else {
                val ends: LocalDateTime = LocalDateTime.parse(sentryplayer.getDuration())
                val now: LocalDateTime = LocalDateTime.now()

                if(now.isEqual(ends) || now.isAfter(ends)) {
                    sentryplayer.unbanPlayer()
                    sentryplayer.addFileEntry(FileEntry(
                        sentryplayer.uniqueId,
                        LocalDate.now().toString(),
                        FileEntryType.UNBAN,
                        "${sentryplayer.name} unbanned")
                    )
                    return
                }
                e.disallow(PlayerLoginEvent.Result.KICK_BANNED, Component.text("${Sentry.PREFIX}\n" +
                        "§cYou have been banned from this network\n" +
                        "§6From: §c${sentryplayer.getInitiator()}\n" +
                        "§6Reason: §c${sentryplayer.getReason()}\n" +
                        "§6Duration: §c${ends.format(sentry.getConfigModel().getConf().getString("Settings.dateFormatPattern")
                            ?.let { DateTimeFormatter.ofPattern(it) })} ${sentry.getConfigModel().getConf().getString("Settings.timezone")}"))
            }
        }
    }
    @EventHandler
    fun onPlayerJoin(e: AsyncPlayerPreLoginEvent) {
        if (!sentry.getDatabaseModel().isUUIDPresent(e.uniqueId)) {
            if (!sentry.getDatabaseModel().isIPv4Present(e.address)) {
                sentry.getDatabaseModel().addToPlayerData(e.uniqueId, "f", e.address)
            } else {
                sentry.getDatabaseModel().addToPlayerData(e.uniqueId, IPv4 = e.address)
                if (sentry.getDatabaseModel().hasMultiAccounts(e.uniqueId)) {
                    Bukkit.getOnlinePlayers().forEach {
                        if (it.hasPermission("sentry.potentialmulti")) {
                            it.sendMessage("${Sentry.PREFIX}§3Potential multiaccount of ${Bukkit.getOfflinePlayer(sentry.getDatabaseModel().getOriginalAccount(e.uniqueId)!!).name}: ${e.name}")
                        }
                    }
                }
            }
        } else {
            if(sentry.getDatabaseModel().hasMultiAccounts(e.uniqueId)) {
                if(Bukkit.getOfflinePlayer(sentry.getDatabaseModel().getOriginalAccount(e.uniqueId)!!).name?.equals(e.name)!!) {
                    return
                }
                Bukkit.getOnlinePlayers().forEach {
                    if (it.hasPermission("sentry.potentialmulti")) {
                        it.sendMessage("${Sentry.PREFIX}§3Potential multiaccount of ${Bukkit.getOfflinePlayer(sentry.getDatabaseModel().getOriginalAccount(e.uniqueId)!!).name}: ${e.name}")
                    }
                }
            }
        }
    }
}