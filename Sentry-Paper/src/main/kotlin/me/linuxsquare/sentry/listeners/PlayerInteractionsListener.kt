package me.linuxsquare.sentry.listeners

import io.papermc.paper.event.player.AsyncChatEvent
import me.linuxsquare.sentry.utils.AFKHandler
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.event.player.PlayerQuitEvent

class PlayerInteractionsListener: Listener {

    @EventHandler
    fun onPlayerJoin(e: PlayerJoinEvent) {
        AFKHandler.update(e.player.uniqueId, AFKHandler.interactingPlayer)
        AFKHandler.update(e.player.uniqueId, AFKHandler.movingPlayer)
        AFKHandler.update(e.player.uniqueId, AFKHandler.chattingPlayer)
    }

    @EventHandler
    fun onPlayerChat(e: AsyncChatEvent) {
        AFKHandler.update(e.player.uniqueId, AFKHandler.chattingPlayer)
    }

    @EventHandler
    fun onPlayerMove(e: PlayerMoveEvent) {
        AFKHandler.update(e.player.uniqueId, AFKHandler.movingPlayer)
    }

    @EventHandler
    fun onPlayerInteract(e: PlayerInteractEvent) {
        AFKHandler.update(e.player.uniqueId, AFKHandler.interactingPlayer)
    }

    @EventHandler
    fun onPlayerLeave(e: PlayerQuitEvent) {
        AFKHandler.update(e.player.uniqueId, AFKHandler.interactingPlayer)
        AFKHandler.update(e.player.uniqueId, AFKHandler.movingPlayer)
        AFKHandler.update(e.player.uniqueId, AFKHandler.chattingPlayer)
    }

}