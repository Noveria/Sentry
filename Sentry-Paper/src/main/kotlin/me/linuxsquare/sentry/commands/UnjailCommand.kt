package me.linuxsquare.sentry.commands

import me.linuxsquare.sentry.Sentry
import me.linuxsquare.sentry.models.FileEntry
import me.linuxsquare.sentry.models.FileEntryType
import net.kyori.adventure.text.Component
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import java.time.LocalDate
import java.util.*

class UnjailCommand(private val sentry: Sentry): CommandExecutor {

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {

        if(!sender.hasPermission("sentry.punishl2")
            && !sender.hasPermission("sentry.punishl3")
            && !sender.hasPermission("sentry.punishl4")) {
            sender.sendMessage("${Sentry.PREFIX}§cYou don't have enough permissions to execute this command!")
            return true
        }

        when(args.size) {
            1 -> {
                unjailPlayer(args[0], sender)
            }
            else -> {
                sender.sendMessage("${Sentry.PREFIX}§cPlease type /unjail <user>")
                return true
            }
        }

        return false
    }

    private fun unjailPlayer(playername: String, initiator: CommandSender) {
        val uuid: String? = sentry.getUniqueID(playername)
        if(uuid == null) {
            initiator.sendMessage("${Sentry.PREFIX}§cThe player §e${playername} §cdoes not exist!")
            return
        }
        val target = Sentry.getSentryOfflinePlayer(UUID.fromString(uuid))

        if(target.name.equals(initiator.name)) {
            initiator.sendMessage("${Sentry.PREFIX}§cYou cannot unjail yourself!")
            return
        }

        if(target.isJailed()) {
            Bukkit.getServer().broadcast(Component.text("${Sentry.PREFIX}§e${playername} §ahas been unjailed"))
            target.unjailPlayer()
            target.addFileEntry(
                FileEntry(
                target.uniqueId,
                LocalDate.now().toString(),
                FileEntryType.UNJAIL,
                "${target.name} unjailed")
            )
            if(target.isOnline) {
                val online = target.player!!
                online.teleport(online.world.spawnLocation)
            }
        } else {
            initiator.sendMessage("${Sentry.PREFIX}§cThe player §e${playername} §chasn't been jailed!")
        }
    }
}