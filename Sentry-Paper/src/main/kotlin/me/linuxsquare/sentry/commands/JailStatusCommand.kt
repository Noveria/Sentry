package me.linuxsquare.sentry.commands

import me.linuxsquare.sentry.Sentry
import me.linuxsquare.sentry.utils.SentryPlayer
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class JailStatusCommand(private val sentry: Sentry): CommandExecutor {

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {

        if(sender !is Player) {
            sender.sendMessage("${Sentry.PREFIX}§cYou can execute this command only as player")
            return true
        }

        val player: SentryPlayer = Sentry.getSentryPlayer(sender.uniqueId)
        if(!player.isJailed()) {
            player.sendMessage("${Sentry.PREFIX}§cYou are currently not jailed!")
            return true
        }

        player.sendMessage("${Sentry.PREFIX}§eJailstatus:\n" +
                "§cJailed by: §e${player.getInitiator()}\n" +
                "§cfor: §e${player.getReason()}\n" +
                "§cuntil: §e${LocalDateTime.parse(player.getDuration()).format(DateTimeFormatter.ofPattern(sentry.getConfigModel().getConf().getString("Settings.dateFormatPattern")))} ${sentry.getConfigModel().getConf().getString("Settings.timezone")}")

        return false
    }
}