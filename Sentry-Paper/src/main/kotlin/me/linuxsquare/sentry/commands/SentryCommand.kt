package me.linuxsquare.sentry.commands

import me.linuxsquare.sentry.Sentry
import me.linuxsquare.sentry.models.FileEntry
import me.linuxsquare.sentry.models.FileEntryType
import me.linuxsquare.sentry.utils.SentryOfflinePlayer
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import java.time.LocalDate
import java.util.*

class SentryCommand(private val sentry: Sentry):CommandExecutor {

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {

        if(!sender.hasPermission("sentry.sentry")) {
            sender.sendMessage("${Sentry.PREFIX}§cYou don't have enough permissions to execute this command!")
            return true
        }

        when(args.size) {
            0 -> {
                sender.sendMessage("${Sentry.PREFIX}§cPlease type /sentry help for help!")
                return true
            }
            1 -> {
                when(args[0]) {
                    "reload" -> {

                        if(!sender.hasPermission("sentry.reload")) {
                            sender.sendMessage("${Sentry.PREFIX}§cYou don't have enough permissions to execute this command!")
                            return true
                        }

                        sentry.getConfigModel().reload()
                        sender.sendMessage("${Sentry.PREFIX}§aConfiguration reloaded")
                    }
                    "setjail" -> {
                        if(sender !is Player) {
                            sender.sendMessage("${Sentry.PREFIX}§cYou can execute this command only as player")
                            return true
                        }

                        if(!sender.hasPermission("sentry.setjail")) {
                            sender.sendMessage("${Sentry.PREFIX}§cYou don't have enough permissions to execute this command!")
                            return true
                        }

                        if(sentry.getDatabaseModel().jailCreated()) {
                           sender.sendMessage("${Sentry.PREFIX}§cThe jail has been already created!")
                            return true
                        }

                        sentry.getDatabaseModel().createJail(sender.location.x, sender.location.y, sender.location.z, sender.location.pitch, sender.location.yaw, sender.location.world.name)
                        sender.sendMessage("${Sentry.PREFIX}§aJail position has been set to ${sender.location.x}/${sender.location.y}/${sender.location.z} in World ${sender.location.world.name}")
                    }
                    "help" -> {

                    }
                    else -> {
                        sender.sendMessage("${Sentry.PREFIX}§cPlease type /sentry <reload/setjail>")
                        return true
                    }
                }
            }
            2 -> {
                when(args[0]) {
                    "approve" -> {

                        if(!sender.hasPermission("sentry.approve")) {
                            sender.sendMessage("${Sentry.PREFIX}§cYou don't have enough permissions to execute this command!")
                            return true
                        }

                        val targetUniqueID = UUID.fromString(sentry.getUniqueID(args[1]))
                        if(targetUniqueID == null) {
                            sender.sendMessage("${Sentry.PREFIX}§cThe player §e${args[1]} §cdoes not exist!")
                            return true
                        }
                        sentry.getDatabaseModel().approveAccount(targetUniqueID)
                        sender.sendMessage("${Sentry.PREFIX}§e${args[1]}'s §aaccount has been verfied, to be a legit multiaccount")
                    }
                }
            }
            3 -> {
                when(args[0]) {
                    "file" -> {
                        when(args[1]) {
                            "delete" -> {
                                if(args[2].matches("[0-9]+".toRegex())) {
                                    if(sentry.getDatabaseModel().deleteFileEntry(Integer.parseInt(args[2]))) {
                                        sender.sendMessage("${Sentry.PREFIX}§aFile-Entry with ID §e${args[2]} §adeleted successfully")
                                    } else {
                                        sender.sendMessage("${Sentry.PREFIX}§cFile-Entry with ID §e${args[2]} §cnot found!")
                                    }
                                } else {
                                    sender.sendMessage("${Sentry.PREFIX}§cYour id-input ${args[2]} is not numeric!")
                                }
                            }
                            "list" -> {
                                val uuid = sentry.getUniqueID(args[2])
                                if(uuid == null) {
                                    sender.sendMessage("${Sentry.PREFIX}§cThe player §e${args[2]} §cdoes not exist!")
                                    return true
                                }
                                val target = Sentry.getSentryOfflinePlayer(UUID.fromString(uuid))

                                val sb = StringBuilder()
                                val header = "${Sentry.PREFIX}§eFile history of §c${target.name}\n"
                                sb.append(header)

                                var i = 0
                                while(i < header.length) {
                                    sb.append("§e=")
                                    if(i == header.length - 1) {
                                        sb.append("\n")
                                    }
                                    i++
                                }

                                for(fileentry in target.getFileEntries()) {
                                    when(fileentry.fileEntryType) {
                                        FileEntryType.INFO -> {
                                            sb.append("§8${fileentry.id} §7- ${fileentry.date} - §b[${fileentry.fileEntryType.type}]§8: §e${fileentry.message}")
                                        }
                                        FileEntryType.KICK,
                                        FileEntryType.JAIL,
                                        FileEntryType.MUTE,
                                        FileEntryType.TEMPBAN,
                                        FileEntryType.BAN -> {
                                            if(fileentry.duration != null) {
                                                sb.append("§8${fileentry.id} §7- ${fileentry.date} - §c[${fileentry.fileEntryType.type} ${fileentry.duration}]§8: §e${fileentry.message}")
                                            } else {
                                                sb.append("§8${fileentry.id} §7- ${fileentry.date} - §c[${fileentry.fileEntryType.type}]§8: §e${fileentry.message}")
                                            }

                                        }
                                        FileEntryType.UNBAN,
                                        FileEntryType.UNJAIL,
                                        FileEntryType.UNMUTE -> {
                                            sb.append("§8${fileentry.id} §7- ${fileentry.date} - §a[${fileentry.fileEntryType.type}]§8: §e${fileentry.message}")
                                        }
                                    }
                                    sb.append("\n")
                                }

                                sender.sendMessage(sb.toString())
                            }
                            "addinfo" -> {
                                sender.sendMessage("${Sentry.PREFIX}§cPlease type /sentry <file> <addinfo> <playername> <message>")
                                return true
                            }
                        }
                    }
                    else -> {
                        sender.sendMessage("${Sentry.PREFIX}§cPlease type /sentry <file> <delete/list> <delete:id/list:playername>")
                        return true
                    }
                }
            }
            else -> {
                when(args[0]) {
                    "file" -> {
                        when(args[1]) {
                            "addinfo" -> {
                                val uuid = sentry.getUniqueID(args[2])
                                if(uuid == null) {
                                    sender.sendMessage("${Sentry.PREFIX}§cThe player §e${args[2]} §cdoes not exist!")
                                    return true
                                }
                                val target = Sentry.getSentryOfflinePlayer(UUID.fromString(uuid))

                                val sb = StringBuilder()

                                var i = 3
                                while(i < args.size) {
                                    sb.append(args[i])
                                    if(i != args.size - 1) {
                                        sb.append(" ")
                                    }
                                    i++
                                }
                                if(target.addFileEntry(FileEntry(
                                        UUID.fromString(uuid),
                                        LocalDate.now().toString(),
                                        FileEntryType.INFO,
                                        sb.toString()))) {
                                    sender.sendMessage("${Sentry.PREFIX}§aFile-Entry for Player §e${target.name} §aadded successfully")
                                } else {
                                    sender.sendMessage("${Sentry.PREFIX}§cFile-Entry for Player §e${target.name} §cfailed to save!")
                                }
                            }
                        }
                    }
                    else -> {
                        sender.sendMessage("${Sentry.PREFIX}§cPlease type /sentry <file> <addinfo> <playername> <message>")
                        return true
                    }
                }
            }
        }

        return false
    }
}