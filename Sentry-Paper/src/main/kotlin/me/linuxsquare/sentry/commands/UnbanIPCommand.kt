package me.linuxsquare.sentry.commands

import me.linuxsquare.sentry.Sentry
import me.linuxsquare.sentry.models.FileEntry
import me.linuxsquare.sentry.models.FileEntryType
import net.kyori.adventure.text.Component
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import java.time.LocalDate
import java.util.*

class UnbanIPCommand(private val sentry: Sentry): CommandExecutor {

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {

        if(!sender.hasPermission("sentry.punishl4")) {
            sender.sendMessage("${Sentry.PREFIX}§cYou don't have enough permissions to execute this command!")
            return true
        }

        when(args.size) {
            1 -> {
                unbanPlayer(args[0], sender)
            }
            else -> {
                sender.sendMessage("${Sentry.PREFIX}§cPlease type /unbanip <user>")
                return true
            }
        }
        return false
    }

    private fun unbanPlayer(playername: String, initiator: CommandSender) {
        val uuid: String? = sentry.getUniqueID(playername)
        if(uuid == null) {
            initiator.sendMessage("${Sentry.PREFIX}§cThe player §e${playername} §cdoes not exist!")
            return
        }
        val target = Sentry.getSentryOfflinePlayer(UUID.fromString(uuid))

        if(target.isIPBanned()) {
            initiator.sendMessage("${Sentry.PREFIX}§aThe player's §e${playername} §aIP has been unbanned")
            target.unbanIP()
        } else {
            initiator.sendMessage("${Sentry.PREFIX}§cThe player's §e${playername} §cIP hasn't been banned!")
        }
    }

}