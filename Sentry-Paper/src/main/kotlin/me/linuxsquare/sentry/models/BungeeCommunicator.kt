package me.linuxsquare.sentry.models

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import me.linuxsquare.sentry.Sentry
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class BungeeCommunicator() {

    companion object {
        private const val USER_AGENT: String = "Mozilla/5.0"
        private var GET_URL: String = "http://${Sentry.getPlugin().getConfigModel().getConf().getString("BungeeCommunicator.host")}:${Sentry.getPlugin().getConfigModel().getConf().getInt("BungeeCommunicator.port")}/${Sentry.getPlugin().getConfigModel().getConf().getString("BungeeCommunicator.apikey")}/"
    }

    private var retMap: HashMap<String, Any> = HashMap()

    fun httpGet(path: String): HashMap<String, Any>? {
        val obj = URL("$GET_URL$path")
        val httpURLConnection: HttpURLConnection = obj.openConnection() as HttpURLConnection
        httpURLConnection.requestMethod = "GET"
        httpURLConnection.setRequestProperty("User-Agent", USER_AGENT)
        val responseCode: Int = httpURLConnection.responseCode
        if(responseCode == HttpURLConnection.HTTP_OK) run {
            val input = BufferedReader(InputStreamReader(httpURLConnection.inputStream))
            val response = StringBuffer()

            response.append(input.readLine())
            input.close()

            val mapType = object : TypeToken<HashMap<String, Any>>() {}.type
            retMap = Gson().fromJson(response.toString(), mapType)

            return retMap
        }
        return null
    }

    fun httpPost(path: String, params: HashMap<String, Any?>) {
        val obj = URL("$GET_URL$path")
        val httpURLConnection: HttpURLConnection = obj.openConnection() as HttpURLConnection

        httpURLConnection.requestMethod = "POST"
        httpURLConnection.setRequestProperty("Content-Type", "application/json; utf-8")
        httpURLConnection.setRequestProperty("User-Agent", USER_AGENT)

        httpURLConnection.doOutput = true
        val wr = httpURLConnection.outputStream
        wr.write(Gson().toJson(params).toByteArray(Charsets.UTF_8))
        wr.close()

        try {
            val input = httpURLConnection.inputStream
            val reader = BufferedReader(InputStreamReader(input))
            var line: String? = null
            while (reader.readLine().also { line = it } != null) {
                println(line)
            }
            input.close()
        } catch (_: Exception) {}


    }

}