package me.linuxsquare.sentry.models

import com.google.gson.GsonBuilder
import okhttp3.*
import okhttp3.internal.http.hasBody
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.util.UUID

class HTTPCommunicator {

    companion object {
        private const val USER_AGENT: String = "Mozilla/5.0"
        private const val GET_URL: String = "https://api.mojang.com/users/profiles/minecraft/"
    }

    private var retMap: HashMap<String, Any> = HashMap()

    data class Data(val id: String, val name: String)

    fun HTTPGet(playername: String): String? {

        val obj = URL("$GET_URL$playername")
        val httpURLConnection: HttpURLConnection = obj.openConnection() as HttpURLConnection
        httpURLConnection.requestMethod = "GET"
        httpURLConnection.setRequestProperty("User-Agent", USER_AGENT)
        val responseCode: Int = httpURLConnection.responseCode
        if(responseCode == HttpURLConnection.HTTP_OK) run {

            val request = Request.Builder().url("$GET_URL$playername").build()
            val client = OkHttpClient()
            val resp = client.newCall(request).execute()
            val gson = GsonBuilder().create()
            return insertDashUUID(gson.fromJson(resp.body?.string(), Data::class.java).id)
        }
        return null
    }

    private fun insertDashUUID(uuid: String): String {
        var sb: StringBuilder = StringBuilder(uuid)
        sb.insert(8,"-")
        sb = StringBuilder(sb.toString())
        sb.insert(13,"-")
        sb = StringBuilder(sb.toString())
        sb.insert(18,"-")
        sb = StringBuilder(sb.toString())
        sb.insert(23, "-")

        return sb.toString()
    }

}