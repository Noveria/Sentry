package me.linuxsquare.sentry.models

import me.linuxsquare.sentry.Sentry
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.configuration.file.YamlConfiguration
import java.io.File

class ConfigModel(private val sentry: Sentry) {

    private var conf: FileConfiguration
    private lateinit var pluginFolder: File
    private lateinit var configFile: File

    init {
        this.conf = this.sentry.config
    }

    fun loadConfig() {
        pluginFolder = File("plugins" + System.getProperty("file.separator") + sentry.description.name)
        if(!pluginFolder.exists()) {
            pluginFolder.mkdir()
        }

        configFile = File(pluginFolder.path + System.getProperty("file.separator") + "config.yml")
        if(!configFile.exists()) {
            sentry.saveDefaultConfig()
        }
    }

    fun reload() {
        configFile = File(pluginFolder.path + System.getProperty("file.separator") + "config.yml")
        conf = YamlConfiguration.loadConfiguration(configFile)
    }

    fun getConf(): FileConfiguration {
        return this.conf
    }

}