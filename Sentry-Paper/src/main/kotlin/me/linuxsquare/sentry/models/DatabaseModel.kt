package me.linuxsquare.sentry.models

import me.linuxsquare.sentry.Sentry
import org.bukkit.Bukkit
import org.bukkit.OfflinePlayer
import org.bukkit.scheduler.BukkitTask
import java.io.File
import java.net.InetAddress
import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.time.LocalDateTime
import java.util.*

class DatabaseModel(sentry: Sentry) {

    private val sentry: Sentry
    private lateinit var con: Connection
    private lateinit var task: BukkitTask

    init {
        this.sentry = sentry
    }

    fun connect() {
        if(sentry.getConfigModel().getConf().getString("Database.type").equals("sqlite", true)) {
            val pluginFolder = File("plugins" + System.getProperty("file.separator") + sentry.description.name)
            if(!pluginFolder.exists()) {
                pluginFolder.mkdir()
            }

            val dbFile = File(pluginFolder.path + System.getProperty("file.separator") + "sentry.db")

            if(!dbFile.exists() || dbFile.length() <= 0) {
                Bukkit.getConsoleSender().sendMessage("${Sentry.PREFIX}§eDatabase sentry not found. Creating...")
                Class.forName("org.sqlite.JDBC")
                setCon(DriverManager.getConnection("jdbc:sqlite:${dbFile}"))
                init()
            } else {
                Class.forName("org.sqlite.JDBC")
                setCon(DriverManager.getConnection("jdbc:sqlite:${dbFile}"))
                Bukkit.getConsoleSender().sendMessage("${Sentry.PREFIX}§aDatabase sentry found")
            }
        } else if(sentry.getConfigModel().getConf().getString("Database.type").equals("mysql", true)) {
            val host = sentry.getConfigModel().getConf().getString("Database.host")
            val port = sentry.getConfigModel().getConf().getString("Database.port")
            val dbname = sentry.getConfigModel().getConf().getString("Database.DBName")
            val username = sentry.getConfigModel().getConf().getString("Database.username")
            val password = sentry.getConfigModel().getConf().getString("Database.password")

            if(username.equals("<YourDBUserHere>", true) || password.equals("<YourDBPasswordHere>", true)) {
                Bukkit.getConsoleSender().sendMessage("${Sentry.PREFIX}§ePlease update your database credentials and then restart your server.")
                Bukkit.getPluginManager().disablePlugin(sentry)
                return
            }

            var dburl = "jdbc:mysql://${host}:${port}"
            setCon(DriverManager.getConnection(dburl,username,password))

            val sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '${dbname}';"
            val stmt = getCon().createStatement()
            var exists = stmt.executeQuery(sql).next()
            if(!exists) {
                Bukkit.getConsoleSender().sendMessage("${Sentry.PREFIX}§eCreating database '${dbname}'")
                val sqlq = "CREATE DATABASE `${dbname}`;"
                stmt.execute(sqlq)
            } else {
                Bukkit.getConsoleSender().sendMessage("${Sentry.PREFIX}§aDatabase '${dbname}' found. Hooking into it...")
            }
            getCon().close()

            dburl = "jdbc:mysql://${host}:${port}/${dbname}"

            Class.forName("com.mysql.jdbc.Driver")
            setCon(DriverManager.getConnection(dburl,username,password))
            exists = getCon().createStatement().executeQuery("SELECT * FROM information_schema.tables WHERE table_schema = '$dbname' AND table_name = 'banList' LIMIT 1;").next()
            if(!exists) {
                Bukkit.getConsoleSender().sendMessage("${Sentry.PREFIX}§eInitializing Database '${dbname}'")
                init()
            }
        }
    }

    fun close() {
        getCon().close()
    }

    private fun init() {
        val stmt = getCon().createStatement()

        val sql = mutableListOf(
            "CREATE TABLE IF NOT EXISTS banList (uuid VARCHAR(40) PRIMARY KEY, initiator VARCHAR(255) NOT NULL, reason VARCHAR(255) NOT NULL, ends VARCHAR(255) NOT NULL DEFAULT 'PERMANENT');",
            "CREATE TABLE IF NOT EXISTS playerData (uuid VARCHAR(40) PRIMARY KEY, status VARCHAR(1) DEFAULT 'm', IPv4 VARCHAR(27) NOT NULL, IPv6 VARCHAR(45) DEFAULT NULL);",
            "CREATE TABLE IF NOT EXISTS muteList (uuid VARCHAR(40) PRIMARY KEY, initiator VARCHAR(255) NOT NULL, reason VARCHAR(255) NOT NULL, ends VARCHAR(255) NOT NULL);",
            "CREATE TABLE IF NOT EXISTS jailList (uuid VARCHAR(40) PRIMARY KEY, initiator VARCHAR(255) NOT NULL, reason VARCHAR(255) NOT NULL, ends VARCHAR(255) NOT NULL, teleported BOOLEAN DEFAULT 0);",
            "CREATE TABLE IF NOT EXISTS jailData (id INT(11) PRIMARY KEY, x DOUBLE NOT NULL, y DOUBLE NOT NULL, z DOUBLE NOT NULL, pitch FLOAT NOT NULL, yaw FLOAT NOT NULL, world VARCHAR(255) NOT NULL);",

        )
        if(sentry.getConfigModel().getConf().getString("Database.type").equals("sqlite", true)) {
            sql.add("CREATE TABLE IF NOT EXISTS banIPList (id INTEGER PRIMARY KEY AUTOINCREMENT, IPv4 VARCHAR(27) NOT NULL, IPv6 VARCHAR(45) DEFAULT NULL);")
            sql.add("CREATE TABLE IF NOT EXISTS fileData (id INTEGER PRIMARY KEY AUTOINCREMENT, uuid VARCHAR(40) NOT NULL, date VARCHAR(10) NOT NULL, type VARCHAR(7) NOT NULL, message VARCHAR(255) NOT NULL, duration VARCHAR(255));")
        } else {
            sql.add("CREATE TABLE IF NOT EXISTS banIPList (id INT(11) PRIMARY KEY AUTO_INCREMENT, IPv4 VARCHAR(27) NOT NULL, IPv6 VARCHAR(45) DEFAULT NULL);")
            sql.add("CREATE TABLE IF NOT EXISTS fileData (id INT(11) PRIMARY KEY AUTO_INCREMENT, uuid VARCHAR(40) NOT NULL, date VARCHAR(10) NOT NULL, type VARCHAR(7) NOT NULL, message VARCHAR(255) NOT NULL, duration VARCHAR(255));")
        }

        sql.forEach {
            stmt.execute(it)
        }

        stmt.close()
    }

    fun jailCreated(): Boolean {
        val jailCreated = "SELECT id FROM jailData WHERE id = 1;"
        val stmt = getCon().createStatement()
        return stmt.executeQuery(jailCreated).next()
    }

    fun createJail(x: Double, y: Double, z: Double, pitch: Float, yaw: Float, world: String) {
        val createJail = "INSERT INTO jailData (id, x, y, z, pitch, yaw, world) VALUES (?,?,?,?,?,?,?);"
        val pstmt = getCon().prepareStatement(createJail)
        pstmt.setInt(1, 1)
        pstmt.setDouble(2, x)
        pstmt.setDouble(3, y)
        pstmt.setDouble(4, z)
        pstmt.setFloat(5, pitch)
        pstmt.setFloat(6, yaw)
        pstmt.setString(7, world)
        pstmt.executeUpdate()
        pstmt.close()
    }

    fun getJail(): ResultSet? {
        val getJail = "SELECT * FROM jailData WHERE id = 1;"
        val stmt = getCon().createStatement()
        val rs = stmt.executeQuery(getJail)
        if(rs.next()) {
            return rs
        }
        return null
    }

    /**
     * @return Boolean - is player banned?
     */
    fun isBanned(offlinePlayer: OfflinePlayer): Boolean {
        val isBanned = "SELECT uuid FROM banList WHERE uuid = ?;"
        val pstmt = getCon().prepareStatement(isBanned)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        return pstmt.executeQuery().next()
    }

    /**
     * Permanent Ban
     */
    fun banPlayer(offlinePlayer: OfflinePlayer, initiator: String, reason: String) {
        val banPlayer = "INSERT INTO banList (uuid, initiator, reason) VALUES (?,?,?);"
        var pstmt = getCon().prepareStatement(banPlayer)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        pstmt.setString(2, initiator)
        pstmt.setString(3, reason)
        pstmt.executeUpdate()

        val updateStatus = "UPDATE playerData SET status = 'b' WHERE uuid = ?"
        pstmt = getCon().prepareStatement(updateStatus)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        pstmt.executeUpdate()

        pstmt.close()
    }

    /**
     * Temp Ban
     */
    fun banPlayer(offlinePlayer: OfflinePlayer, initiator: String, reason: String, ends: String) {
        val banPlayer = "INSERT INTO banList (uuid, initiator, reason, ends) VALUES (?,?,?,?);"
        var pstmt = getCon().prepareStatement(banPlayer)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        pstmt.setString(2, initiator)
        pstmt.setString(3, reason)
        pstmt.setString(4, ends)
        pstmt.executeUpdate()

        val updateStatus = "UPDATE playerData SET status = 'b' WHERE uuid = ?"
        pstmt = getCon().prepareStatement(updateStatus)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        pstmt.executeUpdate()

        pstmt.close()
    }

    fun addToPlayerData(uuid: UUID, status: String = "m", IPv4: InetAddress) {
        val addData = "INSERT INTO playerData (uuid, status, IPv4) VALUES (?,?,?)"
        val pstmt = getCon().prepareStatement(addData)
        pstmt.setString(1, uuid.toString())
        pstmt.setString(2, status)
        pstmt.setString(3, IPv4.toString())
        pstmt.executeUpdate()
        pstmt.close()
    }
    fun addToPlayerData(uuid: UUID, status: String = "m", IPv4: String, IPv6: String) {
        println("$IPv4 : $IPv6")
    }

    fun getPlayerData(uuid: UUID): Map<String, String?> {
        val playerData: MutableMap<String, String?> = mutableMapOf()
        val getPlayerData = "SELECT * FROM playerData WHERE uuid = ?"
        val pstmt = getCon().prepareStatement(getPlayerData)
        pstmt.setString(1, uuid.toString())
        val rs = pstmt.executeQuery()
        while (rs.next()) {
            playerData["uuid"] = rs.getString("uuid")
            playerData["status"] =  rs.getString("status")
            playerData["IPv4"] = rs.getString("IPv4")
            playerData["IPv6"] = rs.getString("IPv6")
        }
        return playerData
    }

    fun isIPv4Present(IPv4: InetAddress): Boolean {
        val isPresent = "SELECT uuid FROM playerData WHERE IPv4 = ?"
        val pstmt = getCon().prepareStatement(isPresent)
        pstmt.setString(1, IPv4.toString())
        return pstmt.executeQuery().next()
    }

    fun isUUIDPresent(uuid: UUID): Boolean {
        val isPresent = "SELECT IPv4 FROM playerData WHERE uuid = ?"
        val pstmt = getCon().prepareStatement(isPresent)
        pstmt.setString(1, uuid.toString())
        return pstmt.executeQuery().next()
    }

    private fun getIPv4fromUUID(uuid: UUID): String {
        val getIPv4 = "SELECT IPv4 FROM playerData WHERE uuid = ?;"
        val pstmt = getCon().prepareStatement(getIPv4)
        pstmt.setString(1, uuid.toString())
        return pstmt.executeQuery().getString(1)
    }

    fun isIPBanned(offlinePlayer: OfflinePlayer): Boolean {
        val isBanned = "SELECT * FROM banIPList WHERE IPv4 = ?"
        var pstmt = getCon().prepareStatement(isBanned)
        pstmt.setString(1, getIPv4fromUUID(offlinePlayer.uniqueId))

        val playerIsBanned = pstmt.executeQuery().next()

        val playerHasF = "SELECT * FROM playerData WHERE uuid = ? AND status = 'f'"
        pstmt = getCon().prepareStatement(playerHasF)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())

        val playerIsF = pstmt.executeQuery().next()

        if(playerIsBanned && playerIsF) {
            return false
        } else if (!playerIsF && playerIsBanned) {
            return true
        }
        return false
    }

    fun getMultiAccounts(uuid: UUID): List<UUID> {
        val multiaccounts = mutableListOf<UUID>()
        val getMultiaccount = "SELECT uuid FROM playerData WHERE status = 'm' AND IPv4 = ?"
        val pstmt = getCon().prepareStatement(getMultiaccount)
        pstmt.setString(1, getIPv4fromUUID(uuid))
        val rs = pstmt.executeQuery()
        while (rs.next()) {
            multiaccounts.add(UUID.fromString(rs.getString("uuid")))
        }
        return multiaccounts
    }

    fun hasMultiAccounts(uuid: UUID): Boolean {
        val isMultiaccount = "SELECT uuid FROM playerData WHERE status = 'm' AND IPv4 = ? LIMIT 1;"
        val pstmt = getCon().prepareStatement(isMultiaccount)
        pstmt.setString(1, getIPv4fromUUID(uuid))
        return pstmt.executeQuery().next()
    }

    fun getOriginalAccount(uuid: UUID): UUID? {
        val getOriginal = "SELECT uuid FROM playerData WHERE status = 'f' AND IPv4 = ? LIMIT 1;"
        val pstmt = getCon().prepareStatement(getOriginal)
        pstmt.setString(1, getIPv4fromUUID(uuid))
        val rs = pstmt.executeQuery()
        while (rs.next()) {
            return UUID.fromString(rs.getString("uuid"))
        }
        return null
    }

    fun approveAccount(uuid: UUID) {
        val approveAccount = "UPDATE playerData SET status = 'f' WHERE uuid = ?"
        val pstmt = getCon().prepareStatement(approveAccount)
        pstmt.setString(1, uuid.toString())
        pstmt.executeUpdate()
        pstmt.close()
    }

    fun banIPPlayerandMultiAccounts(offlinePlayer: OfflinePlayer, initiator: String, reason: String) {

        val playersToBan = mutableListOf<UUID>()

        val getF = "SELECT * FROM playerData WHERE uuid = ? AND status = 'f'"

        var pstmt = getCon().prepareStatement(getF)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        var rs = pstmt.executeQuery()
        while (rs.next()) {
            playersToBan.add(UUID.fromString(rs.getString("uuid")))
        }
        playersToBan.addAll(getMultiAccounts(offlinePlayer.uniqueId))

        val changeStatusByUUID = "UPDATE playerData SET status = 'b' WHERE uuid = ?"
        pstmt = getCon().prepareStatement(changeStatusByUUID)
        playersToBan.forEach {
            pstmt.setString(1, it.toString())
            pstmt.executeUpdate()
        }

        val banUUID = "INSERT OR IGNORE INTO banList(uuid,initiator,reason) VALUES (?,?,?)"
        pstmt = getCon().prepareStatement(banUUID)
        playersToBan.forEach {
            pstmt.setString(1, it.toString())
            pstmt.setString(2, initiator)
            pstmt.setString(3, reason)
            pstmt.executeUpdate()
        }

        val addIPtoBannedIPList = "INSERT OR IGNORE INTO banIPList (IPv4) VALUES (?);"
        pstmt = getCon().prepareStatement(addIPtoBannedIPList)
        pstmt.setString(1, getIPv4fromUUID(offlinePlayer.uniqueId))
        pstmt.executeUpdate()
    }

    /**
     * Unbans a player
     */
    fun unbanPlayer(offlinePlayer: OfflinePlayer) {
        val unbanPlayer = "DELETE FROM banList WHERE uuid = ?;"
        var pstmt = getCon().prepareStatement(unbanPlayer)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        pstmt.executeUpdate()

        val setStatus = "UPDATE playerData SET status = 'm' WHERE uuid = ?"
        pstmt = getCon().prepareStatement(setStatus)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        pstmt.executeUpdate()

        pstmt.close()
    }

    fun unbanIP(offlinePlayer: OfflinePlayer) {

        val ipv4 = getIPv4fromUUID(offlinePlayer.uniqueId)
        if(ipv4.isEmpty()) {
            return
        }

        val unbanIP = "DELETE FROM banIPList WHERE IPv4 = ?"
        val pstmt = getCon().prepareStatement(unbanIP)
        pstmt.setString(1, ipv4)
        pstmt.executeUpdate()
        pstmt.close()
    }

    /**
     * @return String? - null or the person, who banned a player
     */
    fun getBanInitiator(offlinePlayer: OfflinePlayer): String? {
        val banInitiator = "SELECT initiator FROM banList WHERE uuid = ?;"
        val pstmt = getCon().prepareStatement(banInitiator)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        val rs: ResultSet = pstmt.executeQuery()
        if(rs.next()) {
            return rs.getString("initiator")
        }
        return null
    }

    /**
     * @return String? - null or the ban-reason
     */
    fun getBanReason(offlinePlayer: OfflinePlayer): String? {
        val banReason = "SELECT reason FROM banList WHERE uuid = ?;"
        val pstmt = getCon().prepareStatement(banReason)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        val rs: ResultSet = pstmt.executeQuery()
        if(rs.next()) {
            return rs.getString("reason")
        }
        return null
    }

    /**
     * @return String? - null or banduration
     */
    fun getBanDuration(offlinePlayer: OfflinePlayer): String? {
        val banDuration = "SELECT ends FROM banList WHERE uuid = ?;"
        val pstmt = getCon().prepareStatement(banDuration)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        val rs: ResultSet = pstmt.executeQuery()
        if(rs.next()) {
            return rs.getString("ends")
        }
        return null
    }

    /**
     * @return Boolean - is the player muted?
     */
    fun isMuted(offlinePlayer: OfflinePlayer): Boolean {
        val isMuted = "SELECT uuid FROM muteList WHERE uuid = '${offlinePlayer.uniqueId}';"
        val stmt = getCon().createStatement()
        return stmt.executeQuery(isMuted).next()
    }

    /**
     * Mute a player
     */
    fun mutePlayer(offlinePlayer: OfflinePlayer, initiator: String, reason: String, ends: LocalDateTime) {
        val mutePlayer = "INSERT INTO muteList (uuid, initiator, reason, ends) VALUES (?,?,?,?);"
        val pstmt = getCon().prepareStatement(mutePlayer)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        pstmt.setString(2, initiator)
        pstmt.setString(3, reason)
        pstmt.setString(4, ends.toString())
        pstmt.executeUpdate()
        pstmt.close()
    }

    /**
     * Unmute a player
     */
    fun unmutePlayer(offlinePlayer: OfflinePlayer) {
        val unmutePlayer = "DELETE FROM muteList WHERE uuid = ?;"
        val pstmt = getCon().prepareStatement(unmutePlayer)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        pstmt.executeUpdate()
        pstmt.close()
    }

    /**
     * @return String? - null or name of player who muted
     */
    fun getMuteInitiator(offlinePlayer: OfflinePlayer): String? {
        val muteInitiator = "SELECT initiator FROM muteList WHERE uuid = ?;"
        val pstmt = getCon().prepareStatement(muteInitiator)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        val rs: ResultSet = pstmt.executeQuery()
        if(rs.next()) {
            return rs.getString("initiator")
        }
        return null
    }

    /**
     * @return String? - null or mute reason
     */
    fun getMuteReason(offlinePlayer: OfflinePlayer): String? {
        val muteReason = "SELECT reason FROM muteList WHERE uuid = ?;"
        val pstmt = getCon().prepareStatement(muteReason)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        val rs: ResultSet = pstmt.executeQuery()
        if(rs.next()) {
            return rs.getString("reason")
        }
        return null
    }

    /**
     * @return String? - null or duration of mute
     */
    fun getMuteDuration(offlinePlayer: OfflinePlayer): String? {
        val muteDuration = "SELECT ends FROM muteList WHERE uuid = ?;"
        val pstmt = getCon().prepareStatement(muteDuration)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        val rs: ResultSet = pstmt.executeQuery()
        if(rs.next()) {
            return rs.getString("ends")
        }
        return null
    }

    /**
     * @return Boolean - is player jailed?
     */
    fun isJailed(offlinePlayer: OfflinePlayer): Boolean {
        val isJailed = "SELECT uuid FROM jailList WHERE uuid = '${offlinePlayer.uniqueId}';"
        val stmt = getCon().createStatement()
        return stmt.executeQuery(isJailed).next()
    }

    /**
     * Jail a player
     */
    fun jailPlayer(offlinePlayer: OfflinePlayer, initiator: String, reason: String, ends: LocalDateTime) {
        val jailPlayer = "INSERT INTO jailList (uuid, initiator, reason, ends) VALUES (?,?,?,?);"
        val pstmt = getCon().prepareStatement(jailPlayer)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        pstmt.setString(2, initiator)
        pstmt.setString(3, reason)
        pstmt.setString(4, ends.toString())
        pstmt.executeUpdate()
        pstmt.close()
    }

    /**
     * Unjail a player
     */
    fun unjailPlayer(offlinePlayer: OfflinePlayer) {
        val unjailPlayer = "DELETE FROM jailList WHERE uuid = ?;"
        val pstmt = getCon().prepareStatement(unjailPlayer)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        pstmt.executeUpdate()
        pstmt.close()
    }

    /**
     * @return String? - null or player who jailed
     */
    fun getJailInitiator(offlinePlayer: OfflinePlayer): String? {
        val jailInitiator = "SELECT initiator FROM jailList WHERE uuid = ?;"
        val pstmt = getCon().prepareStatement(jailInitiator)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        val rs: ResultSet = pstmt.executeQuery()
        if(rs.next()) {
            return rs.getString("initiator")
        }
        return null
    }

    /**
     * @return String? - null or reason of jail
     */
    fun getJailReason(offlinePlayer: OfflinePlayer): String? {
        val jailReason = "SELECT reason FROM jailList WHERE uuid = ?;"
        val pstmt = getCon().prepareStatement(jailReason)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        val rs: ResultSet = pstmt.executeQuery()
        if(rs.next()) {
            return rs.getString("reason")
        }
        return null
    }

    /**
     * @return String? - null or duration of jail
     */
    fun getJailDuration(offlinePlayer: OfflinePlayer): String? {
        val jailDuration = "SELECT ends FROM jailList WHERE uuid = ?;"
        val pstmt = getCon().prepareStatement(jailDuration)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        val rs: ResultSet = pstmt.executeQuery()
        if(rs.next()) {
            return rs.getString("ends")
        }
        return null
    }

    /**
     * @return Boolean? - null or true/false
     */
    fun hasBeenTeleportedToJail(offlinePlayer: OfflinePlayer): Boolean {
        val hasBeenTeleported = "SELECT teleported FROM jailList WHERE uuid = ?;"
        val pstmt = getCon().prepareStatement(hasBeenTeleported)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        val rs = pstmt.executeQuery()
        if(rs.next()) {
            return rs.getBoolean("teleported")
        }
        return false
    }

    /**
     * Updates teleport to jail bool (only Bungeecord)
     */
    fun updateJailTeleportBool(offlinePlayer: OfflinePlayer) {
        val newJailTeleportBool = "UPDATE jailList SET teleported = ? WHERE uuid = ?"
        val pstmt = getCon().prepareStatement(newJailTeleportBool)
        pstmt.setBoolean(1, true)
        pstmt.setString(2, offlinePlayer.uniqueId.toString())
        pstmt.executeUpdate()
        pstmt.close()
    }

    /**
     * Update jail duration
     */
    fun updateJailDuration(offlinePlayer: OfflinePlayer, ends: LocalDateTime) {
        val newJailDuration = "UPDATE jailList SET ends = ? WHERE uuid = ?"
        val pstmt = getCon().prepareStatement(newJailDuration)
        pstmt.setString(1, ends.toString())
        pstmt.setString(2, offlinePlayer.uniqueId.toString())
        pstmt.executeUpdate()
        pstmt.close()
    }

    /**
     * Add File Entry to Player File
     * @return Boolean
     */
    fun addFileEntry(fileEntry: FileEntry): Boolean {
        val add = "INSERT INTO fileData (uuid, date, type, message, duration) VALUES (?,?,?,?,?);"
        val pstmt = getCon().prepareStatement(add)
        pstmt.setString(1, fileEntry.uuid.toString())
        pstmt.setString(2, fileEntry.date)
        pstmt.setString(3, fileEntry.fileEntryType.toString())
        pstmt.setString(4, fileEntry.message)
        pstmt.setString(5, fileEntry.duration)
        return pstmt.executeUpdate() >= 1
    }

    /**
     * Removes a file entry
     * @return Boolean
     */
    fun deleteFileEntry(fileEntryId: Int): Boolean {
        val delete = "DELETE FROM fileData WHERE id = ?;"
        val pstmt = getCon().prepareStatement(delete)
        pstmt.setInt(1, fileEntryId)
        return pstmt.executeUpdate() >= 1
    }

    /**
     * @return MutableList<FileEntry> - gets all file entries of the named player
     */
    fun getFileEntries(offlinePlayer: OfflinePlayer): MutableList<FileEntry> {
        val get = "SELECT * FROM fileData WHERE uuid = ?;"
        val pstmt = getCon().prepareStatement(get)
        pstmt.setString(1, offlinePlayer.uniqueId.toString())
        val rs = pstmt.executeQuery()

        val fileEntries = mutableListOf<FileEntry>()

        while(rs.next()) {
            fileEntries.add(FileEntry(UUID.fromString(rs.getString("uuid")), rs.getString("date"), FileEntryType.valueOf(rs.getString("type")), rs.getString("message"), rs.getInt("id"), rs.getString("duration")))
        }

        fileEntries.reverse()

        pstmt.close()
        return fileEntries
    }

    fun databaseRefresher() {
        task = Bukkit.getScheduler().runTaskTimerAsynchronously(sentry, Runnable {
            val sql = "SELECT * FROM banList LIMIT 1;"
            val stmt = getCon().createStatement()
            stmt.execute(sql)
            stmt.close()
        }, 0, 20*30)
    }

    fun stopDatabaseRefresher() {
        Bukkit.getScheduler().cancelTask(task.taskId)
    }

    private fun setCon(con: Connection) {
        this.con = con
    }

    private fun getCon(): Connection {
        return this.con
    }

}