package me.linuxsquare.sentry.utils

import org.bukkit.Bukkit
import org.bukkit.command.MessageCommandSender
import org.bukkit.entity.Player
import org.bukkit.profile.PlayerProfile
import java.util.*

open class SentryPlayer(id: UUID) : SentryOfflinePlayer(id), MessageCommandSender {

    private val pl: Player?
    private val uuid: UUID

    init {
        this.pl = Bukkit.getPlayer(id)
        this.uuid = id
    }

    fun isAFK(): Boolean {
        return AFKHandler.isAFK(uuid)
    }

    override fun sendMessage(message: String) {
        pl!!.sendMessage(message)
    }

    override fun isOp(): Boolean {
        return pl!!.isOp
    }

    override fun setOp(p0: Boolean) {
        pl!!.isOp = p0
    }

    override fun getName(): String {
        return pl!!.name
    }

    @Deprecated("'PlayerProfile' is deprecated. Deprecated in Java")
    override fun getPlayerProfile(): PlayerProfile {
        return pl!!.playerProfile
    }
}